import java.util.*;

public class ScannerMultiply {

    public static void main(String[] args) {

	String str = "23 John Smith 42.0 \"Hello World\" $2.50 \"  19\"";

	Scanner console = new Scanner(str);

	while (console.hasNext()) {
	    System.out.println(console.next());
	}

	console.close();

    }

}
