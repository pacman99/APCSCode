public class Unit406_ps {
    public static int[][] magicBox(int x) {
		int[][] box = new int[x][x];
		int max = (x/2)+(x%2);
		for(int c = 1; c <= max; c++)
		    for(int i = c; i <= x-(c+1); i++)
		        for(int j = c; j <= x-(c+1); j++)
			    	box[i][j] = c;
		return box;
    }

	public static int[][] pyramid(int x) {
		x = x+1;
		int[][] b = magicBox(x*2);
		int[][] o = new int[x][x];
		for(int i = 0; i < x; i++)
			for(int j = 0; j < x; j++)
				o[i][j] = b[i][j];
		return o;
	}
				

	public static void printArr(int[][] b) {
		for(int[] i: b) { 
			for(int j: i) { 
				System.out.print(j); 
				System.out.print(" ");
			}
		    System.out.println();
		}
	}

    public static void main(String[] args) {
		System.out.println("Calling magicBox with x=5");
		printArr(magicBox(5));
		System.out.println();

		System.out.println("Calling magicBox with x=11");
		printArr(magicBox(11));
		System.out.println();

		System.out.println("Calling pyramid with x=6");
		printArr(pyramid(6));
		System.out.println();
    }
}
