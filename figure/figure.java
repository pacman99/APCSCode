public class figure {
    public static final int COL = 15;
    public static void top() {
	// Print the first line with "
	System.out.print("|");
	for(int i = 1; i <= COL*2; i++){
	    System.out.print("\"");
	}
	System.out.print("|\n"); // Escape to prepare for next block
	
	for(int i = COL-1; i >= 1; i--){
	    //Print prepending spaces
	    for(int j = 1; j <= COL-i; j++){ //COL-i will cause it increment correctly
	        System.out.print(" ");
	    }
	    //Print the \ then COL*2 number of colons
	    System.out.print("\\");
	    for(int j = 1; j <= i*2; j++){
	        System.out.print(":");
	    }
	    System.out.print("/\n");
	}
    }

    public static void bot() {
	for(int i = 1; i <= COL-1; i++){
	    //Print Prepending spaces: this time it decrements
	    for(int j = 1; j <= COL-i; j++) {
	        System.out.print(" ");
	    }
	    System.out.print("/");
	    //Print appropriate colons and increment it based off of i
	    for(int j = 1; j <= i*2; j++){
	        System.out.print(":");
	    }
	    System.out.print("\\\n");
	}
	//Print last line with " appropriately based on COL
	System.out.print("|");
	for(int i = 1; i <= COL*2; i++){
	    System.out.print("\"");
	}
	System.out.print("|\n");
    }

    public static void main(String[] args) {
	//Start with top, print the middle pipe, print bottom
	top();
	//Print the spaces before the pipes to make it print in the middle
	for(int i=1; i<=COL; i++) {
	    System.out.print(" ");
	}
	System.out.print("||\n");
	bot();
    }

}
