import numpy as np

with open('dummyData1.txt', 'w+') as txt:
    for i in range(10):
        txt.write(f'{np.random.randn(1)}, {np.random.randn(1)}\n')

with open('dummyData2.txt', 'w+') as txt:
    for i in range(10):
        txt.write(f'{np.random.randn(2)}\n')

with open('dummyData1.txt', 'r') as txt:
    line = txt.readline()
    while line:
        print(line)
        line = txt.readline()

with open('dummyData2.txt', 'r') as txt:
    line = txt.readline()
    while line:
        print(line)
        line = txt.readline()
    
