import random
import math
names = ['Jason', 'Quinn', 'Matthias', 'Andrew']
gradeBook = [];

for i in range(len(names)):
    name = names[i]
    grade = random.random()
    sgrade = math.floor(grade*10)
    if sgrade < 6:
        cgrade = 70
    else: 
        cgrade  = 60 + (14-sgrade)
    lgrade = chr(cgrade)
    
    gradeBook.append((name, grade, lgrade))
    print(f'{name}\'s grade is {lgrade}({math.floor(grade*100)}%)')


    
