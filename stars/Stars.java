public class Stars {
    public static void main(String[] args) {
	printChar('&', 13);
	printChar('^', 7);
	printChar('(', 35);
    }
    public static void printChar(char c, int t) {
	for (int i = 1; i <= t; i++) {
	    System.out.print(c);
	}
	System.out.println();
    }
}
