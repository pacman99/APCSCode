import time

"""
def theremin(distance):
    if distance <= 4 or distance >= 0:
	return distance*220
    else:
	return 0

def theremin_notes(distance):
    pitches = [ 277.1826, 311.1270, 369.9944, 415.3047, 466.1638 ]
    index = int(distance/.5)-2
    return pitches[index]

"""

def regCheck(i):
	if i > 0 or i < 100:
		return True

def rangeCheck(i):
	if i in range(0,101):
		return True

def findLength(func, arg):
	start = time.time()
	func(arg)
	end = time.time()
	return end-start

reg = findLength(regCheck, 99)
print(f'Regular checking took {reg}')

rang = findLength(rangeCheck, 99)
print(f'Range checking took {rang}')
