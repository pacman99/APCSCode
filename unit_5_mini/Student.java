//by Parthiv Seetharaman; Period 1; 1/23/20
import java.util.Arrays;

public class Student {
	private String name;
	private int studentID;
	private double[] grades;  //an array with five elements: contain GPA  
	                  //for five courses in a year.
	   
	//default constructor
	public Student() {
        this("John Doe", 0);
	}
	
	//constructor
	//set name and id, and array grades is initialized.
	public Student(String n, int id) {
        name = n;
		studentID = id;
		setGrades(0.0, 0.0, 0.0, 0.0, 0.0);	
	}
		   
    public String getName()  {
        return name;
	}
    public int getID()       {
        return studentID;
	}
	
	//return the grade with index=i 
	public double getGrade(int i)  {
        return grades[i];
	}
	
	//get the average gpa for all 5 classes
	//print a warning message if it is lower than 3.0
	public double getAvgGPA() { 
        double total = 0;
        for(double g : grades) {
            total += g;
        }
        double average = total/5;
        if(average < 3.0) {
            System.out.println("WARNING: Your gpa is low, below 3.0");
        }
        return average;

	}
	
	//return how many classes have gpa < 3.0
	public int numOfFailedClass() {
		int count = 0;
		for(double g : grades) {
			if(g < 3.0) {
				count++;
			}
		}
		return count;
	}
		   
	public void setName(String n) {
		name = n;
	}

	public void setID(int id)     {
		studentID = id;
	}
	
	//set the grade with index=i as g
	public void setGrade(int i, double g) {
		grades[i] = g;
	}
	
	//set all 5 classes' grades as g1,g2,g3,g4,g5
	public void setGrades(double g1, double g2, double g3, double g4, double g5)  {
		grades = new double[] {0.0, 0.0, 0.0, 0.0, 0.0};
	}
	
	//return a string like: name:ID:[3.0,4.0, 3.2, 3.8, 4.0]
	//Please use Arrays.toString(array_name)
	public String toString()  {
		return name + ":" + String.valueOf(studentID) + ":" + Arrays.toString(grades); 
	} 
}