from random import randint
# auto = 'y' in input("Would you like to play by against a computer or friend?[y/n]: ").lower()

# 0, 1, 2 => rock, paper, scissors; respectively
itemref = [ (0, 'rock'), (1, 'paper'), (2, 'scissors') ]

def getItem(text):
    fl = text.lower()[0]
    if fl == 'r':
        return 0
    elif fl == 'p':
        return 1
    elif fl == 's':
        return 2

def randomItem():
    return randint(0,2)

def printItem(item):
    for ref in itemref:
        if ref[0] == item:
            return ref[1]

def getWinner(item1, item2):
    # Returns whether or not item1 wins
    # tie returns none

    if item1 == item2:
        return None
    if abs(item1-item2) == 1:
        if item1 > item2:
            return True
    if abs(item1-item2) == 2:
        if item1 < item2:
            return True
    return False

print("===Rock Paper Scissors===")
while False:
    pchoice = getItem(input("Rock, Paper, or scissors? "))
    cchoice = randomItem()
    print(f"The computer picked {printItem(cchoice)}")
    winner = getWinner(pchoice, cchoice)
    print(pchoice)
    print(cchoice)
    print(winner)
    if winner == None:
        print("Its a tie.")
    else:
        if winner:
            print("Congratulations you won!")
        else:
            print(f"{printItem(cchoice)} beats {printItem(pchoice)}, so you lost, better luck next time")

    notAgain = 'n' in input("Would you like to play again?[y/n]: ")
    if notAgain:
        break



