import java.util.Scanner;

public class TicTacToe {
    //-1 => x; -2 => o
    
    public static final int VERT_PADDING = 1;
    public static final int HORIZ_PADDING = 2;
    public static final boolean DEBUG = false;

    public static void main(String[] args) {
        int[][] board = new int[3][3];
        Scanner input = new Scanner(System.in);
        updateNumbers(board);
        int winner = 0;
        System.out.println("Please choose the number that corresponds with the spot.");
        
        for(int i = 1; i <= 9; i++) {
            printBoard(board);
            int out = -3;
            if(i%2 == 1) {
                //player X(1)
                System.out.println("Player X:");
                out = -1; 
            } else {
                //player O(2)
                System.out.println("Player O:");
                out = -2; 
            }
            int choice = inputId(9-i, input);
            if(DEBUG)
                System.out.println(choice);
            int id = findSpot(board, choice);
            board[id/10][id%10] = out;
            updateNumbers(board);
            boolean winCheck = checkForWin(board, id);
            System.out.println();
            if(winCheck) {
                winner = out;
                break;
            }
        }

        if(winner == 0) {
            System.out.println("*** Its a TIE ***");
        } else if(winner == -1) {
            System.out.println("*** Player X wins ***");
        } else if(winner == -2) {
            System.out.println("*** Player O wins ***");
        } else {
            never();
        }
		
    }

    public static boolean checkForWin(int[][] board, int id) {
        int i = id/10;
        int j = id%10;
        for(int dir = 0; dir <= 7; dir++) {
            if(DEBUG)
                System.out.println("row:" + i + " col:" + j + " dir:" + dir);
            int chance = checkDirection(board, id, dir);
            if(DEBUG) {
                System.out.println("chance: " + chance);
                System.out.println("id: " + id);
            }
            if(chance >= 0) {
                if(DEBUG) 
                    System.out.println("chanceCheck: row:" + chance/10 + " col:" + chance%10 + " dir:" + dir);
                if(checkDirection(board, chance, dir) >= 0) {
                    return true;
                }
                if(DEBUG)
                    System.out.println("chanceCheck: row:" + i + " col:" + j + " dir:" + (dir+4)%7);
                if(checkDirection(board, id, (dir+4)%7) >= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int checkDirection(int[][] board, int id, int direction) {
        int rowd = 0;
        int cold = 0;
        int row = id/10;
        int col = id%10;
        switch(direction) {
            case 0:
                cold = 1;
                break;
            case 1:
                rowd = -1;
                cold = 1;
                break;
            case 2:
                rowd = -1;
                break;
            case 3:
                rowd = -1;
                cold = -1;
                break;
            case 4:
                cold = -1;
                break;
            case 5:
                rowd = 1;
                cold = -1;
                break;
            case 6:
                rowd = 1;
                break;
            case 7:
                rowd = 1;
                cold = 1;
                break;
        }
        rowd += row;
        cold += col;
        if(DEBUG)
            System.out.println(rowd + ", " + cold);
        if((rowd < 0 || rowd > 2) || (cold < 0 || cold > 2)) {
            return -2;
        } else if(board[rowd][cold] != board[row][col]) {
            return -1;
        } else {
            return (rowd)*10+(cold);
        }
    }

    public static int inputId(int max, Scanner input) {
        int choice;
        while(true) {
            System.out.print("=> ");
            choice = input.nextInt();
            if(choice < 0 || choice > max) {
                System.out.println("That was not an option");
                System.out.println("Please pick again");
            } else {
                break;
            }
        }
        return choice; 
    }

    public static int findSpot(int[][] board, int spot) {
        for(int i = 0; i <= 2; i++) {
            for(int j = 0; j <= 2; j++) {
                if(board[i][j] == spot) {
                    return i*10 + j;
                }
            }
        }
        never();
        return -1;
    }        
    
    public static void printBorder(boolean outside) {
        if(outside) {
            System.out.print("|");
            int length = (HORIZ_PADDING * 2 + 2) * 3 - 2;
            for(int i = 0; i <= length; i++) {
                System.out.print("-");
            }
            System.out.println("|");
        } else {   
            for(int i = 0; i <= 2; i++) {
                System.out.print("|");
                padding(false);
                System.out.print(" ");
                padding(false);
            }
            System.out.println("|");
        }
    }

    public static void padding(boolean vertical) {
        if(vertical) {
            for(int i = 0; i < VERT_PADDING; i++)
                printBorder(false);
        } else {
            for(int i = 0; i < HORIZ_PADDING; i++)
                System.out.print(" ");
        }
    }

    public static void updateNumbers(int[][] board) {
        int count = 0;
        for(int i = 0; i <= 2; i++) {
            for(int j = 0; j <= 2; j++) {
                if(board[i][j] >= 0){
                    board[i][j] = count;
                    count++;
                }
            }
        }
    }

    public static void never() {
        System.out.print("**THIS SHOULD NEVVVER HAPAN(HENCE WHY SPELLING DOESN'T MATTER");
    }

    public static void printBoard(int[][] board) {
        printBorder(true);
        for(int[] row : board) {
            padding(true);
            for(int c : row) {
                System.out.print("|");
                padding(false);
                if(c >= 0) {
                    System.out.print(c);
                } else if(c == -1) {
                    System.out.print("X");
                } else if(c == -2) {
                    System.out.print("O");
                } else {
                    never();
                }
                padding(false);
            }
            System.out.println("|");
            padding(true);
            printBorder(true);
        }
    }

            


}

