public double getCompoundAvg(String comp) {
  double total = 0.0;
  int count = 0;
  for(int i = 0; i < trialList.size(); i++) {
    Trial current = trialList.get(i);
    if(current.getCompound() == comp) {
      total += current.getTemp();
      count++;
    }
  }
  if(count == 0) {
    return -1.0;
  }
  return total/count;
}
public String getCompoundWithHighestAvg() {
  ArrayList<String> cList = this.getCompoundList();
  String current = cList.get(0);
  for(int i = 1; i < cList.size(); i++) {
    double newavg = this.getCompoundAvg(cList.get(i));
    if(this.getCompoundAvg(current) < this.getCompoundAvg(cList.get(i))) {
      current = cList.get(i);
    }
  }
  return current;
}




