public class Recursion {
  public static void printNStars(int n) {
    if(n == 0) {
      System.out.println();
    } else {
      System.out.print("*");
      printNStars(n-1);
    }
  }

  public static int factorial(int n) {
    if(n == 1) {
      return 1;
    } else {
      return n * factorial(n-1);
    }
  }

  public static int exp(int n, int p) {
    //n^p
    if(p == 1) {
      return n;
    } else {
      return n * exp(n, p-1);
    }
  }

  public static void main(String[] args) {
    printNStars(5);
    System.out.println(factorial(5));
    System.out.println(exp(5, 3));
  }
}

